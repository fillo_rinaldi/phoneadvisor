const one = document.getElementById('first')
const two = document.getElementById('second')
const three = document.getElementById('third')
const four = document.getElementById('fourth')
const five = document.getElementById('fifth')

const handleSelect = (selection) => {
    switch(selection){
        case 'first' : {
            one.classList.add('checked')
            two.classList.remove('checked')
            three.classList.remove('checked')
            four.classList.remove('checked')
            five.classList.remove('checked')
            return
        }
        case 'second' : {
            one.classList.add('checked')
            two.classList.add('checked')
            three.classList.remove('checked')
            four.classList.remove('checked')
            five.classList.remove('checked')
            return
        }
        case 'third' : {
            one.classList.add('checked')
            two.classList.add('checked')
            three.classList.add('checked')
            four.classList.remove('checked')
            five.classList.remove('checked')
            return
        }
        case 'fourth' : {
            one.classList.add('checked')
            two.classList.add('checked')
            three.classList.add('checked')
            four.classList.add('checked')
            five.classList.remove('checked')
            return
        }
        case 'fifth' : {
            one.classList.add('checked')
            two.classList.add('checked')
            three.classList.add('checked')
            four.classList.add('checked')
            five.classList.add('checked')
            return
        }
    }

}
const getNUmericValue = (stringValue) =>{
    let numericValue;
    if (stringValue === 'first'){
        numericValue = 1
    }
    else if (stringValue  === 'second'){
        numericValue = 2
    }
    else if (stringValue  === 'third'){
        numericValue = 3
    }
    else if (stringValue  === 'fourth'){
        numericValue = 4
    }
    else if (stringValue  === 'fifth'){
        numericValue = 5
    }
    return numericValue
}
if(one) {
    const arr = [one, two,three, four, five]
    arr.forEach(item => item.addEventListener('mouseover', (event) => {
        //cambio stelle accese
        handleSelect(event.target.id)
        const val = event.target.id
        //ottengo numero stelle a seconda delle stelle accese
        const val_num = getNUmericValue(val)
        $('#js_data_input').val(val_num)
    }))
}
//ordina per
$(function() {
    if (localStorage.getItem('order_by')) {
        $("#order-by option").eq(localStorage.getItem('order_by')).prop('selected', true);
    }

    $("#order-by").on('change', function() {
        localStorage.setItem('order_by', $('option:selected', this).index());
    });
    window.onbeforeunload = () => {
        localStorage.removeItem('order_by');
    }
});
