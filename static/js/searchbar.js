//searchbar
$(function() {
    if (localStorage.getItem('categories')) {
        $("#searchbar-categories option").eq(localStorage.getItem('categories')).prop('selected', true);
    }

    $("#searchbar-categories").on('change', function() {
        localStorage.setItem('categories', $('option:selected', this).index());
    });

});