from django.conf.urls.static import static
from django.urls import path
from article import views
from django.conf import settings
app_name = 'article'


urlpatterns = [
        path('add', views.CreateArticleView.as_view(), name='article-add'),
        path('list', views.ListArticleView.as_view(), name='article-list'),
        path('<int:pk>/update', views.UpdateArticleView.as_view(), name='article-update'),
        path('<int:pk>/delete', views.DeleteArticleView.as_view(), name='article-delete'),
        path('<int:pk>/detail', views.ArticleDetail.as_view(), name='article-detail'),
        path('like', views.ajax_like_button, name='article-like'),

]
if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)
