from django.db import models
from django.utils.translation import gettext_lazy as _
# Create your models here.
from smartphone.models import Smartphone
from user_management.models import PlatformUser


class Article(models.Model):
    title = models.CharField(_('Titolo'), max_length=150)
    content = models.TextField(_('Testo'))
    created_by = models.ForeignKey(PlatformUser, on_delete=models.CASCADE, related_name='article_user_creator')
    img = models.ImageField(_("Immagine dell'articolo "), upload_to='article_images',blank=True,null=True)
    publish = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    smartphone = models.ForeignKey(Smartphone, on_delete=models.CASCADE, related_name='comment_article',blank=True,null=True)

    def __str__(self):
        return self.title

    @property
    def category(self):
        return "2"


class Comment(models.Model):
    user = models.ForeignKey(PlatformUser, on_delete=models.CASCADE, related_name='review_user_creator')
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='comment_article')
    title = models.CharField(_('Titolo'), max_length=150)
    description = models.TextField(_('Testo'), max_length=300)
    like = models.ManyToManyField(PlatformUser, related_name='post_likes')

    def __str__(self):
        return self.title

    @property
    def total_likes(self):
        return self.like.count()

