from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

# Create your views here.
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.http import require_POST
from django.views.generic import CreateView, DeleteView, UpdateView, DetailView, ListView
from django.views.generic.edit import FormMixin, FormView

from article.forms import ArticleForm, CommentForm
from article.models import Article, Comment
from article.decorators import article_owner_only


class CreateArticleView(LoginRequiredMixin, CreateView):
    """
    View per la creazione di un nuovo articolo
    """

    template_name = 'article/article_add.html'
    form_class = ArticleForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        id = self.object.pk
        return reverse_lazy('article:article-detail', kwargs={'pk': id})


@method_decorator(article_owner_only, name='dispatch')
class UpdateArticleView(LoginRequiredMixin, UpdateView):
    """
    View per la modifica di un articolo esistente
    """

    template_name = 'article/article_update.html'
    model = Article
    form_class = ArticleForm

    def get_success_url(self):
        id = self.object.pk
        return reverse_lazy('article:article-detail', kwargs={'pk': id})

@method_decorator(article_owner_only, name='dispatch')
class DeleteArticleView(LoginRequiredMixin, DeleteView):
    """
    View per l'eliminazione di un articolo
    """
    model = Article
    success_url = reverse_lazy('article:article-list')


class NewCommentView(LoginRequiredMixin, FormView):
    """
    View per la creazione di un nuovo commento sotto ad un determinato articolo
    """

    form_class = CommentForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.article = Article.objects.get(pk=self.kwargs['pk'])
        self.object.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('article:article-detail', kwargs={'pk': self.kwargs['pk']})


class DetailArticleView(FormMixin, DetailView):
    """
    View per la visualizzazione di un articolo nel dettaglio
    """
    template_name = 'article/article_detail.html'
    model = Article
    form_class = CommentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.get_form()
        context['comments'] = Comment.objects.filter(article=self.object)
        already_liked = []
        for comment in context['comments']:
            if comment.like.filter(id=self.request.user.pk).exists():
                already_liked.append(comment.id)
        context['already_liked'] = already_liked

        return context


class ArticleDetail(View):
    """View che unisce la visualizzazione di un articolo nel dettaglio con un form per inserire un nuovo commento
    sotto all'articolo stesso
    """

    def get(self, request, *args, **kwargs):
        view = DetailArticleView.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = NewCommentView.as_view()
        return view(request, *args, **kwargs)


class ListArticleView(ListView):
    """
    View per la creazione di una lista di articoli presenti sul sito
    """
    model = Article
    template_name = 'article/article_list.html'
    ordering = ['title']

    def get_ordering(self):
        ordering = self.request.GET.get('sort', 'title')
        return ordering


@login_required
@require_POST
def ajax_like_button(request):
    """
    Funzione ajax che implementa l'aggiunta di un like ad un commento
    :param request: richiesta ajax
    :return: risposta utilizzata per aggiornare il numero di like e cambiare colore all'icona
    """
    if request.POST.get("operation") == "like_submit":
        comment_pk = request.POST.get("comment_pk", None)
        comment = get_object_or_404(Comment, pk=comment_pk)
        if comment.like.filter(id=request.user.id):  # already liked the content
            comment.like.remove(request.user)  # remove user from likes
            liked = False
        else:
            comment.like.add(request.user)
            liked = True
        data = {"likes_count": comment.total_likes, "liked": liked, "comment_pk": comment_pk}
        return JsonResponse(data)
