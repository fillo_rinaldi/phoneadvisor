from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse
from article.models import Article, Comment


# Create your tests here.


class AjaxSaveLikeTest(TestCase):
    """
    Test della funzione Ajax 'ajax_like_button'
    """

    def setUp(self):
        """
         Setup di un ambiente di test. Crea i seguenti oggetti a scopo di test:
            - Utente
            - Articolo
            - Commento Articolo
        """
        self.client = Client()
        self.user_1 = get_user_model().objects.create_user('mario', 'mario_rossi@gmail.com', 'mario_password')
        self.user_2 = get_user_model().objects.create_user('luigi', 'luigi_verdi@gmail.com', 'luigi_password')
        self.article = Article.objects.create(created_by=self.user_1,
                                              title='Articolo test',
                                              content='test test test',
                                              )
        self.comment = Comment.objects.create(
            user=self.user_2,
            article=self.article,
            title="Bell'articolo",
            description="Molto interessante"
        )

    def test_ajax_save_like_POST(self):
        """
        Test della POST con utente loggato.
        """
        data = {
            "comment_pk": self.comment.pk,
            'operation': 'like_submit'
        }

        self.client.login(username='luigi', password='luigi_password')
        response = self.client.post(reverse('article:article-like'), data=data)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(Comment.objects.get(pk=self.comment.pk).like.count(), self.comment.total_likes)
        self.assertEquals(self.comment.total_likes, 1)

    def test_ajax_save_like_user_not_logged_in_POST(self):
        """
        Test della POST con utente NON loggato.
        """
        data = {
            "comment_pk": self.comment.pk,
            'operation': 'like_submit'
        }

        response = self.client.post(reverse('article:article-like'), data=data)

        self.assertEquals(response.status_code, 302)
        self.assertEquals(Comment.objects.get(pk=self.comment.pk).like.count(), self.comment.total_likes)
        self.assertEquals(self.comment.total_likes, 0)
