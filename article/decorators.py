from functools import wraps

from django.http import HttpResponseForbidden

from article.models import Article


def article_owner_only(func):
    """Decoratore per negare l'accesso in modifica a utenti non proprietari dell'articolo"""
    @wraps(func)
    def check_and_call(request, *args, **kwargs):
        article = Article.objects.get(pk=kwargs['pk'])
        if not article.created_by == request.user:
            return HttpResponseForbidden()
        return func(request, *args, **kwargs)
    return check_and_call
