from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Div, HTML
from django import forms

from article.models import Article, Comment


class ArticleForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'article-crispy-form'
        self.helper.form_method = 'POST'

        self.helper.layout = Layout(
            Div(
                Row(
                    Column('title', css_class='form-group mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('content', css_class='form-group mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('img', css_class='form-group mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('smartphone', css_class='form-group mb-0'),
                    css_class='form-row'
                ),

                css_class='card-body')
        )

    class Meta:
        model = Article
        fields = ('title', 'content', 'img', 'smartphone')


class CommentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'comment-crispy-form'
        self.helper.form_method = 'POST'

        self.helper.layout = Layout(
            Div(
                Row(
                    Column('title', css_class='form-group mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('description', css_class='form-group mb-0'),
                    css_class='form-row'
                ),

                css_class='card-body')
        )

    class Meta:
        model = Comment
        fields = ('title', 'description')
