import re


def name_to_database(name):
    name = name.upper()
    name = re.compile(r"\s+").sub(" ", name).strip()
    m = re.search(r"\d", name)
    if m is not None and name.find(' ') != m.start() - 1:
        name = name[:m.start()] + '_' + name[m.start():]
    else:
        name = name.translate(str.maketrans(' ', '_'))
    return name


def name_to_template(name):
    name = name.lower()
    name = name.capitalize()
    name = name.translate(str.maketrans('_', ' '))
    return name

'''
def name_attached(name):
    m = re.search(r"\d", name)
    if m is not None and name.find(' ') != m.start() - 1:
        name = name[:m.start()] + '_' + name[m.start():]
    return name

'''