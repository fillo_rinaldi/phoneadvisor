import string
from statistics import mean

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Avg
from django.utils.translation import gettext_lazy as _

from smartphone.utils import name_to_template
from user_management.models import PlatformUser


class Brand(models.Model):
    name = models.CharField(_('Brand'), unique=True, max_length=150)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Smartphone(models.Model):
    created_by = models.ForeignKey(PlatformUser, on_delete=models.CASCADE, related_name='user_creator')
    os_type = (
        ('IOS', 'iOS'),
        ('ANDROID', 'Android')
    )
    name = models.CharField(_('Nome'), max_length=150, unique=True)
    size_width = models.FloatField(_('Larghezza (mm)'), max_length=150)
    size_height = models.FloatField(_('Altezza (mm)'), max_length=150)
    size_depth = models.FloatField(_('Spessore (mm)'), max_length=150)
    os = models.CharField(_('Sistema Operativo'), max_length=10, choices=os_type)
    os_version = models.FloatField(_('Versione'), )
    weight = models.FloatField(_('Peso (g)'), max_length=150)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE, related_name='phone_brand')
    img = models.ImageField(('Immagine dello Smartphone'), upload_to='smartphone_images')

    # NETWORK

    sim_models = (
        ('STANDARD', 'Standard'),
        ('NANO', 'Nano'),
        ('MICRO', 'Micro'),
    )
    sim = models.CharField(max_length=10, choices=sim_models)
    dual_sim = models.BooleanField()
    lte = models.BooleanField(_('LTE'), )
    g_5 = models.BooleanField(_('5G'), )
    max_download_speed = models.IntegerField(_('Max velocità download (Mbps)'))
    min_download_speed = models.IntegerField(_('Min velocità download (Mbps)'), null=True, blank=True)

    # SPECS

    processor = models.CharField(_('Processore'), max_length=50)
    # gpu = models.CharField(_('Scheda Grafica') , max_length=50)
    bit_64 = models.BooleanField(_('64 Bit'))
    ram = models.IntegerField(_('Ram (Gb)'))
    rom = models.IntegerField(_('Memoria max (Gb)'), )
    exp_mem = models.BooleanField(_('Memoria Espandibile '), )
    battery_mah = models.IntegerField(_('Batteria (Mah)'))

    # DISPLAY
    type = (
        ('IPS LCD', 'IPS LCD'),
        ('AMOLED', 'AMOLED'),
        ('OLED', 'OLED'),
    )
    display_inches = models.FloatField(_('Dimensioni schermo (pollici)'), )
    display_resolution_width = models.FloatField(_('Larghezza schermo (px)'))
    display_resolution_height = models.FloatField(_('Altezza schermo (px)'))
    display_type = models.CharField(_('Tipo schermo'), max_length=10, choices=type)
    display_colors = models.IntegerField(_('Colori schermo (milioni)'), default=16)
    display_protection = models.CharField(_('Tipo di vetro'), default='Gorilla Glass 5', max_length=50)

    # CAMERA
    camera_megapixel_rear = models.IntegerField(_('Megapixel camera posteriore'))
    camera_megapixel_front = models.IntegerField(_('Megapixel camera frontale '))

    camera_stabilization = models.BooleanField(_('Stabilizzazione'))
    camera_autofocus = models.BooleanField(_('Autofocus'))
    camera_flash = models.BooleanField(_('Flash'))
    camera_hdr = models.BooleanField(_('HDR'))
    camera_geo_tagging = models.BooleanField(_('Geo Tagging'))
    camera_face_detection = models.BooleanField(_('Face Detection'))

    # VIDEO
    resolution = (
        ('HD', '720 HD'),
        ('FULL HD', '1080 FULL HD'),
        ('4K', '4K'),
        ('8K', '8K'),
    )
    video_resolution_rear = models.CharField(_('Qualità video posteriore'), max_length=10, choices=resolution)
    video_resolution_front = models.CharField(_('Qualità video frontale'), max_length=10, choices=resolution)
    video_autofocus = models.BooleanField(_('Autofocus'))
    video_stabilization = models.BooleanField(_('Stabilizzazione'))
    video_fps = models.IntegerField(_('FPS'))
    video_slowmotion = models.BooleanField(_('SlowMotion'))

    # CONNECTIVITY

    usb_type = (
        ('MICRO USB', 'MICRO USB'),
        ('TYPE C', 'TYPE C'),
        ('LIGHTNING', 'LIGHTNING'),
    )
    wifi = models.BooleanField(_('Wifi'))
    bluetooth = models.BooleanField(_('Bluetooth'))
    nfc = models.BooleanField(_('NFC'))
    gps = models.BooleanField(_('Gps'))
    usb = models.CharField(max_length=10, choices=usb_type)

    # SENSORS

    accelerometer = models.BooleanField(_('Accelerometro'))
    proximity = models.BooleanField(_('Prossimità'))
    gyroscope = models.BooleanField(_('Giroscopio'))
    compass = models.BooleanField(_('Bussola'))
    barometer = models.BooleanField(_('Barometro'))
    fingerprint = models.BooleanField(_('Impronta Digitale'))

    def __str__(self):
        return name_to_template(self.name)

    @property
    def mean_rating(self):
        if Review.objects.filter(smartphone=self).exists():
            return int(self.reviews_smartphone.exclude(rating=None).aggregate(Avg('rating'))['rating__avg'])
        else:
            return 0

    @property
    def category(self):
        return "1"


class Review(models.Model):
    created_by = models.ForeignKey(PlatformUser, on_delete=models.CASCADE, related_name='review_creator', default='1')
    smartphone = models.ForeignKey(Smartphone, on_delete=models.CASCADE, related_name='reviews_smartphone')
    title = models.CharField(_('Titolo'), max_length=150)
    description = models.TextField(_('Testo'), max_length=300)
    rating = models.PositiveSmallIntegerField(_('Stelle'), default=1,
                                              validators=[MinValueValidator(1), MaxValueValidator(5)])

    def __str__(self):
        return self.title
