from functools import wraps

from django.http import HttpResponseForbidden
from smartphone.models import Smartphone


def smartphone_owner_only(func):
    """Decoratore per negare l'accesso in modifica a utenti non proprietari della scheda tecnica"""
    @wraps(func)
    def check_and_call(request, *args, **kwargs):
        smartphone = Smartphone.objects.get(pk=kwargs['pk'])
        if not smartphone.created_by == request.user:
            return HttpResponseForbidden()
        return func(request, *args, **kwargs)
    return check_and_call
