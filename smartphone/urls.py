from django.conf.urls.static import static
from django.urls import path
from smartphone import views
from django.conf import settings
app_name = 'smartphone'


urlpatterns = [
        path('add', views.CreateSmartphoneView.as_view(), name='smartphone-add'),
        path('list', views.ListSmartphoneView.as_view(), name='smartphone-list'),
        path('<int:pk>/update', views.UpdateSmartphoneView.as_view(), name='smartphone-update'),
        path('<int:pk>/delete', views.DeleteSmartphoneView.as_view(), name='smartphone-delete'),
        path('<int:pk>/detail', views.SmartphoneDetail.as_view(), name='smartphone-detail'),
        path('check-smartphone-name', views.check_smartphone_name_is_unique, name='check-smartphone-name'),
        # path('rate', views.rate_smartphone, name='rate-smartphone'),
        # path('search', views.SmartphoneSearchView.as_view(), name='smartphone-search'),
        path('compare/<int:first_pk>/<int:second_pk>/<int:third_pk>', views.CompareSmartphoneView.as_view(), name='smartphone-compare'),

]
if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)
