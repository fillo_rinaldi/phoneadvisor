from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Div, HTML
from django import forms

from smartphone.models import Smartphone, Review


class SmartphoneSpecsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'smartphone-crispy-form'
        self.helper.form_method = 'POST'

        self.helper.layout = Layout(
            Div(
                HTML('''<h4 class="font-bold">GENERALI</h4>'''),
                Div('img', css_class='form-row mb-0  m-2'),
                Div('name', css_class='form-row mb-0  m-2 '),

                Div(css_id='smartphone-check'),

                Div(
                    Div('size_width', css_class='m-2'),
                    Div('size_height', css_class='m-2'),
                    Div('size_depth', css_class='m-2'),
                    css_class='form-row '),
                Div(
                    Div('os', css_class='m-2'),
                    Div('os_version', css_class='m-2'),
                    css_class='form-row '),
                Div('weight', css_class=' m-2 form-row mb-0'),
                Div('brand', css_class=' m-2 form-row mb-0'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),

            Div(
                HTML('''<h4 class="card-title">RETE</h4>'''),
                Div('sim', css_class='m-2 form-row mb-0'),
                Div('dual_sim', css_class='m-2 form-row mb-0'),
                Div('lte', css_class='m-2 form-row mb-0'),
                Div('g_5', css_class='m-2 form-row mb-0'),
                Div('max_download_speed', css_class='m-2 form-row mb-0'),
                Div('min_download_speed', css_class='m-2 form-row mb-0'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),

            Div(
                HTML('''<h4 class="card-title">SPECIFICHE</h4>'''),
                Div('processor', css_class='form-group mb-0 m-2'),
                Div('bit_64', css_class='form-group mb-0 m-2'),
                Div('ram', css_class='form-group mb-0 m-2'),
                Div('rom', css_class='form-group mb-0 m-2'),
                Div('exp_mem', css_class='form-group mb-0 m-2'),
                Div('battery_mah', css_class='form-group mb-0 m-2'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),

            Div(
                HTML('''<h4 class="card-title">DISPLAY</h4>'''),
                Div('display_inches', css_class='form-group mb-0 m-2'),
                Div(
                    Div('display_resolution_width', css_class='m-2'),
                    Div('display_resolution_height', css_class='m-2'),
                    css_class='form-row '),
                Div('display_type', css_class='form-group mb-0 m-2'),
                Div('display_colors', css_class='form-group mb-0 m-2'),
                Div('display_protection', css_class='form-group mb-0 m-2'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),

            Div(
                HTML('''<h4 class="card-title">FOTO</h4>'''),
                Div('camera_megapixel_rear', css_class='form-group mb-0 m-2'),
                Div('camera_megapixel_front', css_class='form-group mb-0 m-2'),
                Div('camera_stabilization', css_class='form-group mb-0 m-2'),
                Div('camera_autofocus', css_class='form-group mb-0 m-2'),
                Div('camera_flash', css_class='form-group mb-0 m-2'),
                Div('camera_hdr', css_class='form-group mb-0 m-2'),
                Div('camera_geo_tagging', css_class='form-group mb-0 m-2'),
                Div('camera_face_detection', css_class='form-group mb-0 m-2'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),

            Div(
                HTML('''<h4 class="card-title">VIDEO</h4>'''),
                Div('video_resolution_rear', css_class='form-group mb-0 m-2'),
                Div('video_resolution_front', css_class='form-group mb-0 m-2'),
                Div('video_autofocus', css_class='form-group mb-0 m-2'),
                Div('video_stabilization', css_class='form-group mb-0 m-2'),
                Div('video_fps', css_class='form-group mb-0 m-2'),
                Div('video_slowmotion', css_class='form-group mb-0 m-2'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),

            Div(
                HTML('''<h4 class="card-title">CONNETTIVITA'</h4>'''),
                Div('wifi', css_class='form-group mb-0'),
                Div('bluetooth', css_class='form-group mb-0 m-2'),
                Div('nfc', css_class='form-group mb-0 m-2'),
                Div('gps', css_class='form-group mb-0 m-2'),
                Div('usb', css_class='form-group mb-0 m-2'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),

            Div(
                HTML('''<h4 class="card-title">SENSORI</h4>'''),
                Div('accelerometer', css_class='form-group mb-0 m-2'),
                Div('proximity', css_class='form-group mb-0 m-2'),
                Div('gyroscope', css_class='form-group mb-0 m-2'),
                Div('compass', css_class='form-group mb-0 m-2'),
                Div('barometer', css_class='form-group mb-0 m-2'),
                Div('fingerprint', css_class='form-group mb-0 m-2'),

                css_class='col-lg-5 col-8 shadow p-1 m-3'),
        )

    class Meta:
        model = Smartphone
        fields = '__all__'
        exclude = ['created_by']


class ReviewForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'review-crispy-form'
        self.helper.form_method = 'POST'

        self.helper.layout = Layout(
            Div(
                Row(
                    Column('title', css_class='form-group mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('description', css_class='form-group mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column(
                        HTML(''' <span class="fa fa-star checked" id="first"></span>
                                             <span class="fa fa-star " id="second"></span>
                                             <span class="fa fa-star " id="third"></span>
                                             <span class="fa fa-star" id="fourth"></span>
                                             <span class="fa fa-star" id="fifth"></span>'''),
                    ),
                ),
                css_class='card-body text-center')
        )

    class Meta:
        model = Review
        fields = ('title', 'description')
