from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView, DetailView, ListView, DeleteView
from django.views.generic.base import View
from django.views.generic.edit import FormMixin, FormView

from smartphone.decorators import smartphone_owner_only
from smartphone.forms import SmartphoneSpecsForm, ReviewForm
from smartphone.models import Smartphone, Review
from smartphone.utils import name_to_database, name_to_template


# Create your views here.

class CreateSmartphoneView(LoginRequiredMixin, CreateView):
    """
    View per la creazione di una nuova scheda tecnica
    """
    template_name = 'smartphone/smartphone_add.html'
    form_class = SmartphoneSpecsForm

    def form_valid(self, form):
        form.instance.name = name_to_database(form.instance.name.upper())
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        id = self.object.pk
        return reverse_lazy('smartphone:smartphone-detail', kwargs={'pk': id})


@method_decorator(smartphone_owner_only, name='dispatch')
class UpdateSmartphoneView(LoginRequiredMixin, UpdateView):
    """
    View per la modifica di una scheda tecnica esistente
    """
    template_name = 'smartphone/smartphone_update.html'
    model = Smartphone
    form_class = SmartphoneSpecsForm

    def form_valid(self, form):
        form.instance.name = name_to_database(form.instance.name.upper())

        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(UpdateSmartphoneView, self).get_form_kwargs()
        kwargs['instance'].name = name_to_template(kwargs['instance'].name)
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'].name = name_to_template(context['object'].name)
        return context

    def get_success_url(self):
        id = self.object.pk
        return reverse_lazy('smartphone:smartphone-detail', kwargs={'pk': id})


@method_decorator(smartphone_owner_only, name='dispatch')
class DeleteSmartphoneView(LoginRequiredMixin, DeleteView):
    """
    View per l'eliminazione di una scheda tecnica
    """
    model = Smartphone
    success_url = reverse_lazy('smartphone:smartphone-list')


class NewReviewView(LoginRequiredMixin, FormView):
    """
    View per la creazione di una nuova opinione sotto ad una specifica scheda tecnica
    """
    form_class = ReviewForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.created_by = self.request.user
        self.object.smartphone = Smartphone.objects.get(pk=self.kwargs['pk'])
        self.object.rating = self.request.POST['rating']
        self.object.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('smartphone:smartphone-detail', kwargs={'pk': self.kwargs['pk']})


class DetailSmartphoneView(FormMixin, DetailView):
    """
    View per la visualizzazione di una scheda tecnica nel dettaglio
    """
    template_name = 'smartphone/smartphone_detail.html'
    model = Smartphone
    form_class = ReviewForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'].name = name_to_template(context['object'].name)
        context['form'] = self.get_form()
        context['compare'] = False
        context['reviews'] = Review.objects.filter(smartphone=self.object)
        return context


class SmartphoneDetail(View):
    """
    View che unisce la visualizzazione di una scheda tecnica con un form per l'inserimento di una nuova opinione sotto
    la scheda
    """

    def get(self, request, *args, **kwargs):
        view = DetailSmartphoneView.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = NewReviewView.as_view()
        return view(request, *args, **kwargs)


class ListReviewView(ListView):
    """
    View per la visualizzazione della lista di opinioni sotto una scheda tecnica
    """
    template_name = 'smartphone/review_list.html'
    model = Review
    ordering = ['created_by']


class ListSmartphoneView(ListView):
    """
    View per la visualizzazione della lista di schede tecniche
    """
    template_name = 'smartphone/smartphone_list.html'
    model = Smartphone
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for object in context['object_list']:
            object.name = name_to_template(object.name)
        return context

    def get_queryset(self):
        ordering = self.request.GET.get('sort', 'name')
        if ordering == 'mean_rating':
            return sorted(Smartphone.objects.all(), key=lambda query: query.mean_rating, reverse=True)
        if ordering == 'nfc':
            return [smartphone for smartphone in Smartphone.objects.filter(nfc=True)]
        else:
            return Smartphone.objects.order_by(ordering)


class CompareSmartphoneView(LoginRequiredMixin, ListView):
    """
    View che implementa il confronto di 3 schede tecniche
    """
    model = Smartphone
    template_name = 'smartphone/compare_smartphone.html'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context['first_clicked'] = False
        context['second_clicked'] = False
        context['third_clicked'] = False

        context['first_list'] = []
        context['second_list'] = []
        context['third_list'] = []

        if self.request.GET.get('search1'):
            query = self.request.GET.get('search1')
            context['first_list'] = Smartphone.objects.filter(name__icontains=name_to_database(query))
        if not context['first_list']:
            context['first_list'] = Smartphone.objects.all()

        if self.request.GET.get('search2'):
            query = self.request.GET.get('search2')
            context['second_list'] = Smartphone.objects.filter(name__icontains=name_to_database(query))
        if not context['second_list']:
            context['second_list'] = Smartphone.objects.all()

        if self.request.GET.get('search3'):
            query = self.request.GET.get('search3')
            context['third_list'] = Smartphone.objects.filter(name__icontains=name_to_database(query))
        if not context['third_list']:
            context['third_list'] = Smartphone.objects.all()

        try:
            first_pk = self.kwargs['first_pk']
            context['first_pk'] = first_pk
            if first_pk != 0:
                context['first_clicked'] = True
                context['first_smartphone'] = Smartphone.objects.get(
                    pk=first_pk)
                context['first_smartphone'].name = name_to_template(context['first_smartphone'].name)

        except Exception as e:
            context['first_smartphone'] = None

        try:
            second_pk = self.kwargs['second_pk']
            context['second_pk'] = second_pk
            if second_pk != 0:
                context['second_clicked'] = True
                context['second_smartphone'] = Smartphone.objects.get(
                    pk=second_pk)

                context['second_smartphone'].name = name_to_template(context['second_smartphone'].name)
        except Exception as e:
            context['second_smartphone'] = None

        try:
            third_pk = self.kwargs['third_pk']
            context['third_pk'] = third_pk
            if third_pk != 0:
                context['third_clicked'] = True
                context['third_smartphone'] = Smartphone.objects.get(
                    pk=third_pk)

                context['third_smartphone'].name = name_to_template(context['third_smartphone'].name)
        except Exception as e:
            context['third_smartphone'] = None

        for object in context['first_list']:
            object.name = name_to_template(object.name)
        for object in context['second_list']:
            object.name = name_to_template(object.name)
        for object in context['third_list']:
            object.name = name_to_template(object.name)

        return context


def check_smartphone_name_is_unique(request):
    """
    Funzione ajax per controllare l'unicità del nome di uno smartphone che si sta inserendo
    :param request: richiesta ajax
    :return: variabile true se il nome è già utilizzato¸ false altrimenti
    """
    smartphone_name = request.GET.get('smartphone_name', False)

    data = {
        'is_used': True if Smartphone.objects.filter(name=name_to_database(smartphone_name)) else False
    }
    return JsonResponse(data)

'''
def rate_smartphone(request, review_pk):
    """
    Funzione per aggiornare ed inserire la valutazione in stelle ad una opinione sotto una scheda tecnica
    :param request: richiesta
    :param review_pk: id della recensione
    :return: variabile che specifica se l'operazione è stata completata con successo e in caso affermativo fornisce
    anche il numero di stelle immesse dall'utente
    """
    if request.method == 'POST':
        val = request.POST.get('val')
        obj = Review.objects.get(id=review_pk)
        obj.rating = val
        obj.save()
        return JsonResponse({'success': 'true', 'score': val}, safe=False)
    return JsonResponse({'success': 'false'})
'''