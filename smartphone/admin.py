from django.contrib import admin
from .models import Brand, Smartphone, Review

# Register your models here.
admin.site.register(Brand)
admin.site.register(Smartphone)
admin.site.register(Review)
