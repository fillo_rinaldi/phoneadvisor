import tempfile
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from smartphone.models import Smartphone, Brand, Review


# Create your tests here.


class SmartphoneDetailViewTest(TestCase):
    """
    Test della view 'SmartphoneDetailView'
    """
    def setUp(self):
        """
        Setup di un ambiente di test. Crea i seguenti oggetti a scopo di test:
            - Utente
            - Smartphone
        :return:
        """
        self.client = Client()
        self.brand = Brand.objects.create(name='APPLE')
        self.user_1 = get_user_model().objects.create_user('mario', 'mario_rossi@gmail.com', 'mario_password')
        self.user_2 = get_user_model().objects.create_user('luigi', 'luigi_verdi@gmail.com', 'luigi_password')
        self.smartphone = Smartphone.objects.create(
            created_by=self.user_1,
            name='Iphone 9',
            size_width='500',
            size_height='900',
            size_depth= '5',
            os = 'iOS',
            os_version = 8,
            weight = '145',
            brand = self.brand,
            img = tempfile.NamedTemporaryFile(suffix=".jpg").name,
            sim = 'Standard',
            dual_sim = True,
            lte = True,
            g_5 = True,
            max_download_speed = 450,
            min_download_speed = 50,
            processor = 'Apple A11',


            bit_64 = True,
            ram = 4,
            rom = 256,
            exp_mem = True,
            battery_mah = 2000,

            #DISPLAY
            display_inches = 6.6,
            display_resolution_width = '1340',
            display_resolution_height = '600',

            display_type = 'IPS LCD',
            display_colors = '16 milioni',
            display_protection = 'Gorilla Glass 5',

            # CAMERA
            camera_megapixel_rear = 12,
            camera_megapixel_front = 5,

            camera_stabilization = True,
            camera_autofocus =True,
            camera_flash = True,
            camera_hdr = True,
            camera_geo_tagging = True,
            camera_face_detection = False,

            # VIDEO

            video_resolution_rear = '1080 FULL HD',
            video_resolution_front = '1080 FULL HD',
            video_autofocus = True ,
            video_stabilization = True,
            video_fps = 64,
            video_slowmotion =True,

            # CONNECTIVITY
            wifi = True,
            bluetooth =True,
            nfc = True,
            gps =True,
            usb = 'LIGHTNING' ,

            # SENSORS
            accelerometer = True,
            proximity = True,
            gyroscope = True,
            compass = False,
            barometer = True,
            fingerprint = True,
        )


    def test_smartphone_page_view_user_owner_of_smartphone_GET(self):
        """
        Test della GET della pagina con utente proprietario della scheda tecnica dello smartphone.
        """

        review = Review.objects.create(
            created_by=self.user_2,
            smartphone=self.smartphone,
            title='Telefono molto bello',
            description=' Veramente un bel telefono ricco di funzionalità',
            rating=5,
        )

        response = self.client.get(reverse('smartphone:smartphone-detail', kwargs={ 'pk' : self.smartphone.pk }))
        self.assertEquals(response.status_code, 200)

        user_login = self.client.login(username='mario', password='mario_password')
        self.assertTrue(user_login)
        response = self.client.get(reverse('smartphone:smartphone-detail', kwargs={'pk': self.smartphone.pk}))
        self.assertEquals(response.status_code, 200)

        self.assertTemplateUsed(response, 'smartphone/smartphone_detail.html')
        self.assertNotContains(response,'Nessuna recensione presente')
        self.assertContains(response, "Modifica scheda")

        self.assertEquals(response.context['object'], self.smartphone)
        self.assertQuerysetEqual(response.context['reviews'],['<Review: Telefono molto bello>'])

    def test_smartphone_page_view_user_non_owner_of_smartphone_empty_reviews_list_GET(self):
        """
        Test della GET della pagina con utente non proprietario della scheda tecnica dello smartphone e nessuna
        recensione presente.
        """

        response = self.client.get(reverse('smartphone:smartphone-detail', kwargs={ 'pk' : self.smartphone.pk }))
        self.assertEquals(response.status_code, 200)

        user_login = self.client.login(username='luigi', password='luigi_password')
        self.assertTrue(user_login)
        response = self.client.get(reverse('smartphone:smartphone-detail', kwargs={'pk': self.smartphone.pk}))
        self.assertEquals(response.status_code, 200)


        self.assertContains(response,'Nessuna recensione presente')
        self.assertNotContains(response, "Modifica scheda")

        self.assertEquals(response.context['object'], self.smartphone)
        self.assertQuerysetEqual(response.context['reviews'],[])

    def test_smartphone_page_view_user_not_authenticated_GET(self):
        """
        Test della GET della pagina con utente non autenticato
        """

        review = Review.objects.create(
            created_by=self.user_2,
            smartphone=self.smartphone,
            title='Telefono molto bello',
            description=' Veramente un bel telefono ricco di funzionalità',
            rating=5,
        )

        response = self.client.get(reverse('smartphone:smartphone-detail', kwargs={ 'pk' : self.smartphone.pk }))
        self.assertEquals(response.status_code, 200)

        response = self.client.get(reverse('smartphone:smartphone-detail', kwargs={'pk': self.smartphone.pk}))
        self.assertEquals(response.status_code, 200)


        self.assertNotContains(response,'Nessuna recensione presente')
        self.assertNotContains(response, "Lascia una recensione")

        self.assertEquals(response.context['object'], self.smartphone)
        self.assertQuerysetEqual(response.context['reviews'],['<Review: Telefono molto bello>'])

    def test_smartphone_page_view_new_comment_user_logged_POST(self):
        """
        Test della POST della pagina con utente loggato.
        """
        data = {
            'title': 'Telefono molto bello',
            'description': 'Veramente un bel telefono ricco di funzionalità',
            'rating': 5,
        }

        user_login = self.client.login(username='mario', password='mario_password')
        self.assertTrue(user_login)

        response = self.client.post(reverse('smartphone:smartphone-detail', kwargs={'pk': self.smartphone.pk}), data=data)
        self.assertEquals(response.status_code, 302)
        self.assertEquals(Review.objects.filter(smartphone=self.smartphone).count(),1)
        self.assertEquals(Review.objects.filter(title='Telefono molto bello').count(), 1)


    def test_smartphone_page_view_new_comment_user_not_logged_POST(self):
        """
        Test della POST della pagina con utente non loggato.
        """
        data = {
            'created_by': self.user_2,
            'smartphone': self.smartphone,
            'title': 'Telefono molto bello',
            'description': 'Veramente un bel telefono ricco di funzionalità',
            'rating': 5,
        }


        response = self.client.post(reverse('smartphone:smartphone-detail', kwargs={'pk': self.smartphone.pk}),data=data)
        self.assertEquals(response.status_code, 302)
        self.assertEquals(Review.objects.filter(smartphone=self.smartphone).count(),0)
        self.assertEquals(Review.objects.filter(title='Telefono molto bello').count(), 0)