# Progetto di tecnologie web PhoneAdvisor##

Project made by:
  • Filippo Rinaldi, matr. 123889

Requirements: 
 - Python 3.X
 - Pip
 - Pipenv (recommended)

 Because I have used Python3, I run only "pip" and "python", but if you have both Python2 and Python3
 installed you should use "pip3" and "python3" instead of "pip" and "python", because the last two
 would be for Python2.

Packages used:
- Django==3.1.6
- django-crispy-forms==1.11.0
- Pillow==8.0.1
- (asgiref==3.3.1)
- (pytz==2021.1)
- (sqlparse==0.4.1)


For a faster and cleaner configuration it is recommended to use pipenv


---Initial configuration---

1) Install pipenv by running the following command:
    $ pip install pipenv

2) Go to the project base folder 

3) Run the follwing command to install all the required packages
    $ pipenv install

4) Launch the newly created environment
    $ pipenv shell

---Local server start and site visit---

1) From the base folder run the following command
    python manage.py runserver

2) Open your favorite browser and go to http://127.0.0.1:8000
