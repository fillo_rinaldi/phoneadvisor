from django.conf.urls.static import static
from django.urls import path
from user_management import views
from django.conf import settings
app_name = 'user_management'


urlpatterns = [
        path('registration', views.UserCreateView.as_view(), name='user-register'),
        path('login', views.LoginUserView.as_view(), name='user-login'),
        path('logout', views.LogoutUserView.as_view(), name='user-logout'),
        path('list', views.ListUserView.as_view(), name='user-list'),
        path('profile/<int:pk>', views.ProfileView.as_view(), name='user-overview'),
        path('profile/<int:pk>/update', views.UserUpdateView.as_view(), name='user-update'),
        path('profile/<int:pk>/delete', views.UserDeleteView.as_view(), name='user-delete'),
        path('personal-profile/create', views.UserProfileCreateView.as_view(), name='user-profile-create'),
        path('personal-profile/<int:pk>/update', views.UserProfileUpdateView.as_view(), name='user-profile-update'),
        path('personal-profile/<int:pk>/delete', views.UserProfileDeleteView.as_view(), name='user-profile-delete'),
        path('check-username', views.check_username_is_unique, name='check-username'),

]

