from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Div
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django import forms
from django.utils.translation import gettext_lazy as _

from user_management.models import PlatformUser, Profile


class RegistrationForm(UserCreationForm):
    """
    Registration form where the user entering: username, email, name, last name
    and password can create a new profile to access the application
    """
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = 'example@example.com'
        self.fields['username'].widget.attrs['placeholder'] = 'Username'
        self.fields['password1'].widget.attrs['placeholder'] = 'Password'
        self.fields['password2'].widget.attrs['placeholder'] = _('repeat password')
        self.fields['first_name'].widget.attrs['placeholder'] = _('first name')
        self.fields['last_name'].widget.attrs['placeholder'] = _('last name')
        self.helper = FormHelper()
        self.helper.form_id = 'utente-crispy-form'
        self.helper.form_method = 'POST'

        self.helper.layout = Layout(
            Row(
                Column('username', css_class='form-group m-3'),

                Div(css_id='username-check'),

                Column('first_name', css_class='form-group m-3'),
                Column('last_name', css_class='form-group m-3'),
                css_class='form-row'
            ),
            Row(
                Column('email', css_class='form-group m-3'),
                css_class='form-row'
            ),
            Row(
                Column('password1', css_class='form-group m-3'),
                Column('password2', css_class='form-group m-3'),
                css_class='form-row'
            ),
        )

    class Meta:
        model = PlatformUser
        fields = ("email", "username", "first_name", "last_name", 'password1', 'password2')



class LoginForm(AuthenticationForm):
    """
    Login form where an already registered user can enter the username and password to authenticate
    """

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'utente-crispy-form'
        self.helper.form_method = 'POST'
        self.request = request
        self.user_cache = None

        self.helper.layout = Layout(
            Row(
                Column('username', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('password', css_class='form-group mb-0'),
                css_class='form-row'
            ),
        )


class UserProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'utente-crispy-form'
        self.helper.form_method = 'POST'

    class Meta:
        model = Profile
        fields = ('birthday', 'profile_img')
