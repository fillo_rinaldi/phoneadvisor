from django.contrib.auth import login
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView, DetailView, DeleteView, ListView

from user_management.decorators import user_owner_only, has_profile_only, has_not_profile
from user_management.forms import LoginForm, RegistrationForm, UserProfileForm
from user_management.models import PlatformUser, Profile


class UserCreateView(CreateView):
    """
    View per la creazione di un nuovo utente
    """
    form_class = RegistrationForm
    template_name = 'registration/registration_form.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super(UserCreateView, self).form_valid(form)
        login(self.request, self.object)
        return response


class LoginUserView(LoginView):
    """
    View che implementa la schermata di login
    """
    form_class = LoginForm
    template_name = 'registration/login.html'
    success_url = reverse_lazy('home')


class LogoutUserView(LogoutView):
    """
    View che implementa la schermata di logout
    """
    template_name = 'registration/logged_out.html'
    success_url = reverse_lazy('user_management:user-login')


class ProfileView(DetailView):
    """
    View per visualizzare una panoramica dell'account di un utente
    """
    template_name = 'user_management/user_overview.html'
    model = PlatformUser


class UserUpdateView(UpdateView):
    """
    View per la modifica di un utente esistente
    """
    template_name = 'user_management/user_update.html'
    model = PlatformUser
    form_class = RegistrationForm

    def get_success_url(self):
        user_id = self.kwargs['pk']
        return reverse_lazy('user_management:user-overview', kwargs={'pk': user_id})


@method_decorator(user_owner_only, name='dispatch')
class UserDeleteView(DeleteView):
    """
    View per l'eliminazione di un utente
    """

    model = PlatformUser
    success_url = reverse_lazy('user_management:user-login')


@method_decorator(has_not_profile, name='dispatch')
class UserProfileCreateView(CreateView):
    """
    View per la creazione di un profilo utente
    """
    template_name = 'user_management/user_profile_create.html'
    form_class = UserProfileForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(UserProfileCreateView, self).form_valid(form)

    def get_success_url(self):
        user_id = self.request.user.pk
        return reverse_lazy('user_management:user-overview', kwargs={'pk': user_id})


@method_decorator(user_owner_only, name='dispatch')
@method_decorator(has_profile_only, name='dispatch')
class UserProfileUpdateView(UpdateView):
    """
    View per la modifica di un profilo utente
    """
    template_name = 'user_management/user_profile_update.html'
    form_class = UserProfileForm
    model = PlatformUser

    def get_object(self, queryset=None):
        return Profile.objects.get(pk=self.request.user.profile.pk)

    def get_success_url(self):
        user_id = self.request.user.pk
        return reverse_lazy('user_management:user-overview', kwargs={'pk': user_id})


@method_decorator(user_owner_only, name='dispatch')
@method_decorator(has_profile_only, name='dispatch')
class UserProfileDeleteView(DeleteView):
    """
    View per l'eliminazione del profilo (ma non dell'account) di un utente
    """

    model = Profile

    def get_object(self, queryset=None):
        return Profile.objects.get(pk=self.request.user.profile.pk)

    def get_success_url(self):
        user_id = self.request.user.pk
        return reverse_lazy('user_management:user-overview', kwargs={'pk': user_id})


class ListUserView(ListView):
    """
    View per la visualizzazione di una lista di utenti
    """
    template_name = 'user_management/user_list.html'
    model = PlatformUser
    ordering = ['username']

    def get_queryset(self):
        ordering = self.request.GET.get('sort', 'username')
        if ordering != 'has_profile':
            return PlatformUser.objects.order_by(ordering).exclude(pk=1)
        else:
            return [user for user in PlatformUser.objects.all().exclude(pk=1) if user.has_profile]


def check_username_is_unique(request):
    """
    Funzione ajax che controlla l'unicità dell'username scelto da un utente in fase di registrazione
    o di modifica dell'account
    :param request: richiesta ajax
    :return: variabile true se l'username è gia utilizzato false altrimenti
    """
    username = request.GET.get('username', False)
    data = {
        'is_used': True if PlatformUser.objects.filter(username=username) else False
    }
    return JsonResponse(data)
