from functools import wraps

from django.http import HttpResponseForbidden
from smartphone.models import Smartphone
from user_management.models import PlatformUser


def user_owner_only(func):
    """
    Decoratore per negare l'accesso in modifica a utenti non proprietari dell'account
    """

    @wraps(func)
    def check_and_call(request, *args, **kwargs):
        user = PlatformUser.objects.get(pk=kwargs['pk'])
        if not user == request.user:
            return HttpResponseForbidden()
        return func(request, *args, **kwargs)

    return check_and_call


def has_profile_only(func):
    """
    Decoratore per negare l'accesso in modifica a utenti non proprietari del profilo
    """

    @wraps(func)
    def check_and_call(request, *args, **kwargs):
        user = PlatformUser.objects.get(pk=kwargs['pk'])
        if not user.has_profile:
            return HttpResponseForbidden()
        return func(request, *args, **kwargs)

    return check_and_call


def has_not_profile(func):
    """
    Decoratore per negare la visualizzazioone della pagina di creazione di un profilo se l'utente ne ha già uno
    """

    @wraps(func)
    def check_and_call(request, *args, **kwargs):
        user = PlatformUser.objects.get(pk=request.user.pk)
        if user.has_profile:
            return HttpResponseForbidden()
        return func(request, *args, **kwargs)

    return check_and_call
