from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils.translation import gettext_lazy as _
from PhoneAdvisor import settings


class PlatformUser(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)

    # AbstractUser._meta.get_field('email')._unique = True

    class Meta:
        verbose_name = _('platform_user')
        verbose_name_plural = _('platform_users')

    @property
    def has_profile(self):
        try:
            assert self.profile
            return True
        except ObjectDoesNotExist:
            return False

    @property
    def category(self):
        return "3"


class Profile(models.Model):
    birthday = models.DateField(_('Compleanno YYYY-MM-DD'), null=True, blank=True)
    profile_img = models.ImageField(('Immagine del Profilo'), upload_to='profile_images')
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
