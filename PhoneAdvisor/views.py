from itertools import chain

from django.views.generic import TemplateView, ListView

from article.models import Article
from smartphone.models import Smartphone, Review
from smartphone.utils import name_to_database, name_to_template
from user_management.models import PlatformUser


class HomePageView(TemplateView):
    """
    View per la visualizzazione della homepage con annessi 5 migliori smartphone e 5 articoli più recenti
    """
    template_name = 'homepage.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["recent_smartphones"] = sorted(Smartphone.objects.all(), key=lambda query: query.mean_rating,
                                               reverse=True)[:5]
        context["recent_articles"] = Article.objects.order_by('-publish')[:5]
        return context


class SearchView(ListView):
    """
    View che implementa la searchbar presente nella navbar per la ricerca di schede tecniche, articoli e utenti
    """
    model = Smartphone
    template_name = 'results_list.html'

    def get_queryset(self):

        query = self.request.GET.get('search')
        value_category = self.request.GET.get('category')
        results = None
        if query:
            if "0" in value_category:
                results = list(chain(Smartphone.objects.filter(name__icontains=name_to_database(query)) |
                                        Smartphone.objects.filter(created_by__username__icontains=query),
                                     Article.objects.filter(title__icontains=(query)) | \
                                     Article.objects.filter(content__icontains=query) | Article.objects.filter(
                                         created_by__username__icontains=query),
                                     PlatformUser.objects.filter(username__icontains=query)))
            if "1" in value_category:
                results = Smartphone.objects.filter(name__icontains=name_to_database(query))
            if "2" in value_category:
                results = Article.objects.filter(title__icontains=(query)) | \
                          Article.objects.filter(content__icontains=query)
            if '3' in value_category:
                results = PlatformUser.objects.filter(username__icontains=query) | \
                          PlatformUser.objects.filter(email__icontains=query)

        else:
            if "0" in value_category:
                results = list(chain(Smartphone.objects.all(), Article.objects.all(), PlatformUser.objects.all()))

            if "1" in value_category:
                results = Smartphone.objects.all()
            if "2" in value_category:
                results = Article.objects.all()
            if "3" in value_category:
                results = PlatformUser.objects.all()

        return results

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = self.request.GET.get('category')
        context['query'] = self.request.GET.get('search')
        if context['object_list']:
            for object in context['object_list']:
                if __class__.__name__ == 'Smartphone':
                    object.name = name_to_template(object.name)
        return context
